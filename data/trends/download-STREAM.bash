#!/usr/bin/env bash

DB_URL="http://www.cs.virginia.edu/stream/FTP/Tables/stream.db"
DB_OUTFILE="STREAM--all.csv"

if test -f "${DB_OUTFILE}" ; then
  echo "'${DB_OUTFILE}' exists; saving a copy in '${DB_OUTFILE}.orig'..."
  cp "${DB_OUTFILE}" "${DB_OUTFILE}.orig"
fi

echo "Downloading STREAM database from ${DB_URL} ..."
if ! wget "${DB_URL}" ; then
  echo '*** ERROR while downloading! ***'
  exit 1
fi

DB_DOWNLOADED=$(basename "${DB_URL}")
dos2unix "${DB_DOWNLOADED}"

echo "Creating: ${DB_OUTFILE} ..."
echo "Name,nCPUs,BytesPerWord,COPY,SCALE,ADD,TRIAD,MHz,FLOPperCy,IsShared,IsDistr,IsUni,IsOther,BenchType,IsObsolete,SysType,Date,URL" > "${DB_OUTFILE}"
cat "${DB_DOWNLOADED}" >> "${DB_OUTFILE}"

rm -f "${DB_DOWNLOADED}"
echo 'Done!'

# eof
